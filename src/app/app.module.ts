import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { chartpieComponent } from './chartpie/chartpie.component';
import { ChartModule } from 'angular2-chartjs';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule }    from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [chartpieComponent],
  imports: [
    BrowserModule,
    ChartModule,
    ChartsModule,
    HttpClientModule,
    NgbModule.forRoot()],
  entryComponents: [chartpieComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    const customButton = createCustomElement(chartpieComponent, {
      injector: injector,
     });
     customElements.define('app-chartpie', customButton);
  }

  ngDoBootstrap() {}
}
