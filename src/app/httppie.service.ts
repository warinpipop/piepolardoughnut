import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


interface itempic{
  id:number;
  name:string;
  amount:number;
  type:string;
}
@Injectable({
  providedIn: 'root'
})
export class HttppieService {

  constructor(private http:HttpClient) { }
  getChartPie(url): Observable<any> {
    console.log(url)
    return this.http.get<itempic>(url)
  }
}
