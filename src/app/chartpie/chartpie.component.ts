import { Component,  ViewEncapsulation, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { HttppieService } from '../httppie.service';


@Component({
  selector: 'app-chartpie',
  template: `

  <button (click)='getdata()'>Reset</button>
  <div  class='example-loading-shade' *ngIf="isLoadingResults">
  <div *ngIf="isLoadingResults">{{err}}</div>
</div>
 <div class="container">
 <div class="row">
 <div class='col'>
  <chart baseChart 
        [data]="pieChartData"
        [type]="pieChartType"
        >
        </chart>
</div>
</div>
</div>
        
  `,
  styles: [
    `.example-loading-shade {
      position: absolute;
      top: 0;
      left: 0;
      bottom:0;
      right: 0;
      background: rgba(0, 0, 0, 0.15);
      z-index: 1;
      display: flex;
      align-items: center;
      justify-content: center;
    }`
  ],
  encapsulation: ViewEncapsulation.Native
})
export class chartpieComponent implements OnInit{

  constructor(private http: HttppieService) { } 
   
  pieChartData: any;
  pieChartLabels: any;
  companyName: any;
  companyamount: any;
  pieChartType: string;
  isLoadingResults = true;
  types:string;
  err:string;
   @Input() url='';

ngOnInit() {
    this.getdata();
  }
  getdata(){
  this.http.getChartPie(this.url)
  .subscribe(
    (data) => {
      this.isLoadingResults = false;
      this.types= data[0].type;
      console.log(this.types)
      var name = [];
      var amount = []
      for (let i = 0; i < data.length;i++) {
        name.push(data[i].name)
        amount.push(data[i].amount)
      }
      this.companyName = name;
      this.companyamount = amount;

    },
    (err) => {
      this.isLoadingResults = true;
      this.err=err
    },
    () => {
      
      this.pieChartType=this.types
      this.pieChartData = {
        datasets: [{
          data: this.companyamount,
          backgroundColor: randomColor()
        }],
  
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: this.companyName
      };
  
  
      function randomColor() {
        var colors = ['#28a745', '#145A32', '#008080', '#17a2b8', '#007bff', '#6610f2', '#e83e8c', '#dc3545', '#fd7e14', '#ffc107']
        var color = [];
        for (let i = 0; i < 10; i++) {
          color.push(colors[i]);
        }
        return color
      }
    });
  }
}

  
